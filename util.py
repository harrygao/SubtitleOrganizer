import os
import re
import shutil
from typing import List

subtitle_ext_list = ['ass', 'srt']
video_ext_list = ['mp4', 'mkv', 'avi']


def get_file_name_without_ext(file_name: str) -> str:
    split_result = os.path.splitext(file_name)
    return split_result[0]


def get_file_ext(file_name: str) -> str:
    split_result = os.path.splitext(file_name)
    return split_result[1]


def is_file_with_ext(file_name: str, ext_list: List[str]) -> bool:
    for ext in ext_list:
        if file_name.__contains__(ext):
            return True
    return False


def is_subtitle_file(file_name: str) -> bool:
    subtitle_ext_list = ['ass', 'srt']
    return is_file_with_ext(file_name, subtitle_ext_list)


def is_video_file(file_name: str) -> bool:
    movie_ext_list = ['mp4', 'mkv', 'avi']
    return is_file_with_ext(file_name, movie_ext_list)


def dir_contain_file_with_ext(directory: str, ext_list: List[str]) -> bool:
    files = os.listdir(directory)
    for file in files:
        if is_file_with_ext(file, ext_list):
            return True
    return False


def dir_contain_subtitle_file(directory: str) -> bool:
    return dir_contain_file_with_ext(directory, subtitle_ext_list)


def get_episode(file_name: str) -> str:
    regex = "S\d+E(\d+)"
    x = re.search(regex, file_name)
    return x.group(1)


def check_subtitle_folder_with_same_episode(subtitle_folder: str, episode: str) -> str | None:
    files = os.listdir(subtitle_folder)
    for file in files:
        if file.__contains__(f"E{episode}") and is_subtitle_file(file):
            return file
    return None


def copy_subtitle_file_with_video(delete_original_subtitle_file: bool, file_name: str, src_dir: str,
                                  dst_dir: str) -> str:
    src_path = os.path.join(src_dir, file_name)
    dst_path = os.path.join(dst_dir, file_name)
    return shutil.copyfile(src_path, dst_path)


def rename_subtitle_file_as_same_as_video(target_dir: str, video_file_name: str, subtitle_file_name: str) -> bool:
    video_file_without_ext = get_file_name_without_ext(video_file_name)
    subtitle_ext = get_file_ext(subtitle_file_name)
    new_subtitle_file_path = os.path.join(target_dir, f"{video_file_without_ext}{subtitle_ext}")
    original_file_path = os.path.join(target_dir, subtitle_file_name)
    os.rename(original_file_path, new_subtitle_file_path)
    return True
