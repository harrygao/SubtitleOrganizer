# This is a sample Python script.
import os
import sys
from typing import List

from util import *


def auto_detect_the_first_folder_with_subtitle(target_dir: str) -> str | None:
    list_dir = os.listdir(target_dir)
    for file in list_dir:
        sub_file_root_path = os.path.join(target_dir, file)
        if not os.path.isdir(sub_file_root_path):
            continue
        if dir_contain_subtitle_file(sub_file_root_path):
            # this is the folder that we're looking for
            return sub_file_root_path
    return None


def check_movie_file_one_by_one(target_dir: str, subtitle_folder: str):
    files = os.listdir(target_dir)
    for video_file in files:
        print(f"start processing file {video_file}")
        is_file = os.path.isfile(os.path.join(target_dir, video_file))
        if is_file and is_video_file(video_file):
            episode_str = get_episode(video_file)
            match_subtitle_file_name = check_subtitle_folder_with_same_episode(subtitle_folder, episode_str)
            copy_subtitle_file_with_video(False, match_subtitle_file_name, subtitle_folder, target_dir)
            rename_result = rename_subtitle_file_as_same_as_video(target_dir, video_file, match_subtitle_file_name)
            if rename_result:
                print(f"move file {match_subtitle_file_name} to {target_dir} success")
            else:
                print(f"something wrong")


def main():
    # print(sys.argv)
    target_dir = sys.argv[1]
    delete_original_subtitle = False
    is_user_input_valid = os.path.isdir(target_dir)
    if not is_user_input_valid:
        print("the path you input is not directory")
        return

    # check if there are a folder contained with ass or srt files
    subtitle_folder = auto_detect_the_first_folder_with_subtitle(target_dir)
    print(subtitle_folder)
    check_movie_file_one_by_one(target_dir, subtitle_folder)


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    main()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
